ARG GOLANG_IMAGE="golang:1-alpine"
ARG GOOS=linux
ARG GOARCH=amd64
ARG GOARM
ARG GOPROXY

FROM ${GOLANG_IMAGE} AS build
WORKDIR /build
ARG GOPROXY
ENV GOPROXY=${GOPROXY}
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
ARG GOOS
ARG GOARCH
ARG GOARM
RUN CGO_ENABLED=0 GOOS=${GOOS} GOARCH=${GOARCH} GOARM=${GOARM} go build -o gomodproxy ./cmd/gomodproxy

FROM ${GOLANG_IMAGE}
COPY --from=build /build/gomodproxy /usr/local/bin/gomodproxy
